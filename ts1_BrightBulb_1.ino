// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full
// license information.

// This gist is based on https://github.com/Azure/iot-central-firmware/blob/master/ESP8266/ESP8266.ino


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN    D2

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 6

#define MY_SW D7

bool fadingUp = false;
int pwmValue = 0;

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);


#include <ESP8266WiFi.h>
#include "src/iotc/common/string_buffer.h"
#include "src/iotc/iotc.h"

#define WIFI_SSID "LANdownUnder"
#define WIFI_PASSWORD "mitgrimmenet123"

const char* SCOPE_ID = "0ne001C611E";
const char* DEVICE_ID = "1h7r1llv1im";
const char* DEVICE_KEY = "TLtvsG3AY3IdqkY3qcV52LTQEHXhHsrGXFEmKWzUgPA=";

void on_event(IOTContext ctx, IOTCallbackInfo* callbackInfo);
#include "src/connection.h"

void on_event(IOTContext ctx, IOTCallbackInfo* callbackInfo) {
  // ConnectionStatus
  if (strcmp(callbackInfo->eventName, "ConnectionStatus") == 0) {
    LOG_VERBOSE("Is connected ? %s (%d)",
                callbackInfo->statusCode == IOTC_CONNECTION_OK ? "YES" : "NO",
                callbackInfo->statusCode);
    isConnected = callbackInfo->statusCode == IOTC_CONNECTION_OK;
    return;
  }

  // payload buffer doesn't have a null ending.
  // add null ending in another buffer before print
  AzureIOT::StringBuffer buffer;
  if (callbackInfo->payloadLength > 0) {
    buffer.initialize(callbackInfo->payload, callbackInfo->payloadLength);
  } 
  LOG_VERBOSE("- [%s] event was received. Payload => %s\n",
              callbackInfo->eventName, buffer.getLength() ? *buffer : "EMPTY");
  
      
  if (strcmp(callbackInfo->eventName, "Command") == 0) {
    LOG_VERBOSE("- Command name was => %s\r\n", callbackInfo->tag);
      fadingUp = true;
      pwmValue = 1;
    
  }
  
  if (strcmp(callbackInfo->eventName, "SettingsUpdated") == 0) {
    LOG_VERBOSE("- Setting name was => %s\r\n", callbackInfo->tag);
  }
}
int counter = 0;
int brightBulbID = 1;

void sendAzureMsg(){
  char msg[128] = {0};
  int pos = 0, errorCode = 0;
  pos = snprintf(msg, sizeof(msg) - 1, "{\"Bright Bulb ID\": %d, \"Counter\":%d}", brightBulbID, counter++);
  errorCode = iotc_send_telemetry(context, msg, pos);

  msg[pos] = 0;

  if (errorCode != 0) {
    LOG_ERROR("Sending message has failed with error code %d", errorCode);
  }
}


void handleButtonFade(){
  if(fadingUp == false && pwmValue == 0){ // awaiting button press, while no fading is happening
    if(digitalRead(MY_SW) == LOW){ // button is active low (press will make output go LOW)
      fadingUp = true;
      pwmValue = 1;
      sendAzureMsg();
    }
  }
  else if(pwmValue != 0){ // awaiting led fade to finish
    if(fadingUp == true){
      pwmValue++;
    }
    else{
      pwmValue--;
    }
    if(pwmValue == 255){ // change direction of fading
      fadingUp = false;
    }
    
    for(int i=0; i<strip.numPixels(); i++) {                              // For each pixel in strip...
      strip.setPixelColor(i,strip.Color(pwmValue,pwmValue>>1,pwmValue>>2));     // Set pixel's color (in RAM)
    }
    strip.show();                                                         // Update strip to match
    //analogWrite(MY_LED, pwmValue);
  }
}
bool isConnectedOld = false;

void setup() {
  
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  Serial.begin(9600);

  for(int i=0; i<strip.numPixels(); i++) {          // For each pixel in strip...
    strip.setPixelColor(i,strip.Color(0,0,20));     // Set pixel's color (in RAM)
  }
  strip.show();                                     // Update strip to match
      
  connect_wifi(WIFI_SSID, WIFI_PASSWORD);
  connect_client(SCOPE_ID, DEVICE_ID, DEVICE_KEY);

  if (context != NULL) {
    lastTick = 0;  // set timer in the past to enable first telemetry a.s.a.p
  }
}

void loop() {
  if (isConnected) {
    unsigned long ms = millis();
    if (ms - lastTick > 50) {  // send telemetry every 5 seconds
      lastTick = ms;
      handleButtonFade();
    }
    iotc_do_work(context);  // do background work for iotc
  } else {
    iotc_free_context(context);
    context = NULL;
    connect_client(SCOPE_ID, DEVICE_ID, DEVICE_KEY); 
  }
  if(isConnectedOld == false && isConnected){
    strip.clear();
    strip.show();
    isConnectedOld = true;
  }
  else if(isConnectedOld == true && isConnected == false) {
    for(int i=0; i<strip.numPixels(); i++) {          // For each pixel in strip...
      strip.setPixelColor(i,strip.Color(0,0,20));     // Set pixel's color (in RAM)
    }
    strip.show();                                     // Update strip to match
    isConnectedOld = false;
  }
}
